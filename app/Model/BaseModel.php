<?php
declare(strict_types=1);

namespace App\Model;
use Nette;
use Nette\Database\Table\Selection;

abstract class BaseModel
{
    private $dbexplorer;
    private $tableName;

    public function __construct(Nette\Database\Explorer $dbexplorer)
    {
        $this->dbexplorer = $dbexplorer;
        $this->tableName = $this->getTable();
    }

    abstract function getTable();

    public function Insert($data) : void
    {
        $this->dbexplorer->table($this->tableName->insert($data));
    }

public function Update(int $id, $data) : void
{
    $this->dbexplorer->table($this->tableName)->where("id",$id)->update($data);
}

public function Delete($id) : void
{
    $this->dbexplorer->table($this->tableName)->where("id",$id)->delete();
}


}






?>
