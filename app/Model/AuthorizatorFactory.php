<?php
namespace App\Model;
use Nette;

class AuthorizatorFactory
{
    public static function create(): Nette\Security\Permission
    {
        $acl = new Nette\Security\Permission;
        $acl->addRole('amdin');
        $acl->addRole('moderator');
        $acl->addResource('backend');
        $acl->allow('admin','backend');
        $acl->allow('moderator','backend','view');
        return $acl;
    }
}
?>